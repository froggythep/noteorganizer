package org.itam.noteorganizer.wizards;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.itam.noteorganizer.internal.GlobalDefines;

public class SettingsWizard extends Wizard {

	private SettingsWizardPage settingsWizardPage;

	@Override
	public String getWindowTitle() {
		return "Settings";
	}

	@Override
	public void addPages() {
		settingsWizardPage = new SettingsWizardPage();
		addPage(settingsWizardPage);
	}

	@Override
	public boolean performFinish() {
		if (isSaveDirectoryChanged() || isArchiveDirectoryChanged()) {
			if (MessageDialog.openQuestion(new Shell(Display.getCurrent()), "Move current notes?", "Current directory contains notes. Should they be moved into the new directory?")) {
				moveNotes();
			}
		}

		if (isSaveDirectoryChanged()) {
			GlobalDefines.setSaveLocation(getSelectedSaveDirectory(), true);
		}
		if (isArchiveDirectoryChanged()) {
			GlobalDefines.setArchiveLocation(getSelectedArchiveDirectory(), true);
		}
		return true;
	}

	public boolean isSaveDirectoryChanged() {
		return ! GlobalDefines.getSaveLocation().equals(settingsWizardPage.getSelectedSaveDirectory());
	}

	public boolean isArchiveDirectoryChanged() {
		return ! GlobalDefines.getArchiveLocation().equals(settingsWizardPage.getSelectedArchiveDirectory());
	}

	public String getSelectedSaveDirectory() {
		return settingsWizardPage.getSelectedSaveDirectory();
	}

	public String getSelectedArchiveDirectory() {
		return settingsWizardPage.getSelectedArchiveDirectory();
	}

	private void moveNotes() {
		if (isSaveDirectoryChanged()) {
			moveSavedNotes();
		}
		if (isArchiveDirectoryChanged()) {
			moveArchivedNotes();
		}
	}

	private void moveSavedNotes() {
		movePaths(getPathsToMove(GlobalDefines.getSaveLocation()), getSelectedSaveDirectory());
	}

	private void moveArchivedNotes() {
		movePaths(getPathsToMove(GlobalDefines.getArchiveLocation()), getSelectedArchiveDirectory());

	}

	private Collection<Path> getPathsToMove(final String path) {
		try (Stream<Path> paths = Files.walk(Paths.get(path), 1)) {
			return paths.filter(Files::isRegularFile).collect(Collectors.toSet());
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return Collections.emptySet();
	}

	private void movePaths(final Collection<Path> pathsToMove, final String to) {
		pathsToMove.forEach(path -> {
			path.toFile().renameTo(new File(to + path.getFileName()));
		});
	}
}
