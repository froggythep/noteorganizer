package org.itam.noteorganizer.wizards;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.itam.noteorganizer.adapter.SelectionAdapter;
import org.itam.noteorganizer.internal.GlobalDefines;

public class SettingsWizardPage extends WizardPage {

	private enum DirectoryType { SAVE_DIRECTORY, ARCHIVE_DIRECTORY };

	//UI-Elements
	private Text txtSavedNotes;
	private Button btnSearchSavedNotes;

	private Text txtArchivedNotes;
	private Button btnSearchArchivedNotes;

	//ACTIONS
	private SelectDirectoryAction selectSavedDirectoryAction;
	private SelectDirectoryAction selectArchivedDirectoryAction;

	//HELPER
	private String selectedSaveDirectory;
	private String selectedArchiveDirectory;

	protected SettingsWizardPage() {
		super("Settings");
		selectedSaveDirectory = GlobalDefines.getSaveLocation();
		selectedArchiveDirectory = GlobalDefines.getArchiveLocation();
	}

	@Override
	public String getTitle() {
		return "Settings";
	}

	@Override
	public String getDescription() {
		return "You can edit the settings here";
	}

	@Override
	public void createControl(final Composite parent) {
		parent.setLayout(new GridLayout(2, false));
		parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final Label lblSavedNotes = new Label(parent, SWT.NONE);
		lblSavedNotes.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));
		lblSavedNotes.setText("Folder for saved notes:");

		txtSavedNotes = new Text(parent, SWT.BORDER);
		txtSavedNotes.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		txtSavedNotes.setEditable(false);

		btnSearchSavedNotes = new Button(parent, SWT.PUSH);
		btnSearchSavedNotes.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		btnSearchSavedNotes.setText("Change");

		final Label lblArchivedNotes = new Label(parent, SWT.NONE);
		lblArchivedNotes.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));
		lblArchivedNotes.setText("Folder for archived notes:");

		txtArchivedNotes = new Text(parent, SWT.BORDER);
		txtArchivedNotes.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		txtArchivedNotes.setEditable(false);

		btnSearchArchivedNotes = new Button(parent, SWT.PUSH);
		btnSearchArchivedNotes.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		btnSearchArchivedNotes.setText("Change");

		setControl(parent);

		makeActions();
		addListeners();
		initData();
	}

	private void makeActions() {
		selectSavedDirectoryAction = new SelectDirectoryAction(DirectoryType.SAVE_DIRECTORY);
		selectArchivedDirectoryAction = new SelectDirectoryAction(DirectoryType.ARCHIVE_DIRECTORY);
	}

	private void addListeners() {
		btnSearchSavedNotes.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent arg0) {
				selectSavedDirectoryAction.run();
			}
		});

		btnSearchArchivedNotes.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent arg0) {
				selectArchivedDirectoryAction.run();
			}
		});
	}

	private void initData() {
		txtSavedNotes.setText(GlobalDefines.getSaveLocation());
		txtArchivedNotes.setText(GlobalDefines.getArchiveLocation());
	}

	private class SelectDirectoryAction extends Action {

		final DirectoryType directoryType;

		public SelectDirectoryAction(final DirectoryType directoryType) {
			this.directoryType = directoryType;
		}

		@Override
		public void run() {
			final DirectoryDialog dialog = new DirectoryDialog(new Shell(Display.getCurrent()));
			dialog.setMessage("Select a directory");
			final String path = dialog.open();

			if (path != null) {
				switch (directoryType) {
				case SAVE_DIRECTORY:
					selectedSaveDirectory = path + "\\";
					txtSavedNotes.setText(selectedSaveDirectory);
					break;
				case ARCHIVE_DIRECTORY:
					selectedArchiveDirectory = path + "\\";
					txtArchivedNotes.setText(selectedArchiveDirectory);
					break;
				default:
					break;
				}
			}
		}
	}

	public String getSelectedSaveDirectory() {
		return selectedSaveDirectory;
	}

	public String getSelectedArchiveDirectory() {
		return selectedArchiveDirectory;
	}

}
