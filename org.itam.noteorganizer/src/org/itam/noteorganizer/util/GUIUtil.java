package org.itam.noteorganizer.util;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.TableViewer;

public class GUIUtil {

	public static void hookContextMenu(final TableViewer viewer, final Action... actions) {
		final MenuManager manager = new MenuManager("#PopupMenu");

		manager.setRemoveAllWhenShown(true);
		manager.addMenuListener(new IMenuListener() {

			@Override
			public void menuAboutToShow(final IMenuManager manager) {
				for (final Action action : actions) {
					manager.add(action);

				}
			}
		});

		viewer.getTable().setMenu(manager.createContextMenu(viewer.getTable()));
	}
}
