package org.itam.noteorganizer.filter;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.itam.noteorganizer.entities.Note;

public class TextFilter extends ViewerFilter {
	
	private String searchString;
	
	public void setSearchText(String s) {
        this.searchString = ".*" + s + ".*";
    }

	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		
		if (searchString == null || searchString.length() == 0) {
            return true;
        }
		
		if (element instanceof Note) {
			Note note = (Note) element;
			return note.getNoteTitle().toLowerCase().matches(searchString.toLowerCase());
		}
		return true;
	}

}
