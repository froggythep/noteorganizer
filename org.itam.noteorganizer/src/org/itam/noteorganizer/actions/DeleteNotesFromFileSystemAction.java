package org.itam.noteorganizer.actions;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import org.eclipse.jface.action.Action;
import org.itam.noteorganizer.entities.Note;
import org.itam.noteorganizer.internal.GlobalDefines;

public class DeleteNotesFromFileSystemAction extends Action {

	public enum DeleteMode { ARCHIVE, DELETE_COMPLETELY };

	DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");

	final private Collection<Note> notesToDelete = new HashSet<>();
	final private DeleteMode deleteMode;

	public DeleteNotesFromFileSystemAction(final Note noteToDelete, final DeleteMode deleteMode) {
		notesToDelete.add(noteToDelete);
		this.deleteMode = deleteMode;
	}

	public DeleteNotesFromFileSystemAction(final Collection<Note> notesToDelete, final DeleteMode deleteMode) {
		this.notesToDelete.addAll(notesToDelete);
		this.deleteMode = deleteMode;
	}

	@Override
	public void run() {
		notesToDelete.forEach(note -> {
			if (note != null) {
				final Path path = Paths.get(GlobalDefines.getSaveLocation() + note.getNoteTitle() + ".txt");
				final File file = path.toFile();
				if (file.exists()) {
					switch (deleteMode) {
					case DELETE_COMPLETELY:
						file.delete();
						break;
					case ARCHIVE:
						file.renameTo(new File(GlobalDefines.getArchiveLocation() + note.getNoteTitle() + "_DELETED_" + getCurrentDateString()));
						break;
					default:
						break;
					}
				}
			}
		});
	}

	private String getCurrentDateString() {
		final Date date = new Date();
		return dateFormat.format(date);
	}
}
