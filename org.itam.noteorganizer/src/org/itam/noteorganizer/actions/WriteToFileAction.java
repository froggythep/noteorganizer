package org.itam.noteorganizer.actions;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.jface.action.Action;
import org.itam.noteorganizer.entities.Note;
import org.itam.noteorganizer.internal.GlobalDefines;

public class WriteToFileAction extends Action {

	final Note note;

	public WriteToFileAction(final Note note) {
		this.note = note;
	}

	@Override
	public void run() {
		final Path path = Paths.get(GlobalDefines.getSaveLocation() + note.getNoteTitle() + ".txt");

		final StringBuilder builder = new StringBuilder();
		builder.append(GlobalDefines.NOTE_ID_START_IDENTIFIER + "\n");
		builder.append(note.getId() + "\n");
		builder.append(GlobalDefines.NOTE_ID_END_IDENTIFIER + "\n");

		builder.append(GlobalDefines.NOTE_TITLE_START_IDENTIFIER + "\n");
		builder.append(note.getNoteTitle() + "\n");
		builder.append(GlobalDefines.NOTE_TITLE_END_IDENTIFIER + "\n");

		builder.append(GlobalDefines.NOTE_TEXT_START_IDENTIFIER + "\n");
		builder.append(note.getNote() + "\n");
		builder.append(GlobalDefines.NOTE_TEXT_END_IDENTIFIER + "\n");

		final byte[] strToBytes = builder.toString().getBytes();

		try {
			final File file = path.toFile();
			if (! file.exists()) {
				file.createNewFile();
			}
			Files.write(path, strToBytes);
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}
}
