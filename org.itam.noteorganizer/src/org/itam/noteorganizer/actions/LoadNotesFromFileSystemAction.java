package org.itam.noteorganizer.actions;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Stream;

import org.eclipse.jface.action.Action;
import org.itam.noteorganizer.entities.Note;
import org.itam.noteorganizer.internal.GlobalDefines;

public class LoadNotesFromFileSystemAction extends Action {

	final private Collection<Note> notes = new HashSet<>();

	@Override
	public void run() {
		notes.clear();
		try (Stream<Path> paths = Files.walk(Paths.get(GlobalDefines.getSaveLocation()), 1)) {
			paths.filter(Files::isRegularFile).forEach(filePath -> {
				try (final BufferedReader br = new BufferedReader(new FileReader(filePath.toString()))) {
					final String noteId = getStringForStartAndEndIdentifier(br, GlobalDefines.NOTE_ID_START_IDENTIFIER, GlobalDefines.NOTE_ID_END_IDENTIFIER);
					final String noteTitle = getStringForStartAndEndIdentifier(br, GlobalDefines.NOTE_TITLE_START_IDENTIFIER, GlobalDefines.NOTE_TITLE_END_IDENTIFIER);
					final String note = getStringForStartAndEndIdentifier(br, GlobalDefines.NOTE_TEXT_START_IDENTIFIER, GlobalDefines.NOTE_TEXT_END_IDENTIFIER);
					notes.add(new Note(Long.valueOf(noteId), noteTitle, note));

				} catch (final Exception e) {
					//something went terribly wrong
					e.printStackTrace();
				}
			});
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	private String getStringForStartAndEndIdentifier(final BufferedReader br, final String startIdentifier, final String endIdentifier) throws IOException {
		final StringBuilder builder = new StringBuilder();

		String line = br.readLine();
		while (line != null) {
			if (line.equals(startIdentifier)) {
				line = br.readLine();
				while ( ! line.equals(endIdentifier)) {
					builder.append(line);
					line = br.readLine();
					if ( ! line.equals(endIdentifier)) {
						builder.append(System.lineSeparator());
					} else {
						return builder.toString();
					}
				}
			}
			line = br.readLine();
		}

		return builder.toString();
	}

	public Collection<Note> getNotes() {
		return notes;
	}
}
