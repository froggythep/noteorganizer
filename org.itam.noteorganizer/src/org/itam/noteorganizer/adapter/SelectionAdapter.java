package org.itam.noteorganizer.adapter;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

public class SelectionAdapter implements SelectionListener {

	@Override
	public void widgetDefaultSelected(final SelectionEvent arg0) {
		//override if necessary
	}

	@Override
	public void widgetSelected(final SelectionEvent arg0) {
		//override if necessary
	}

}
