package org.itam.noteorganizer.internal;

public class GlobalDefines {

	public final static String NOTE_ID_START_IDENTIFIER = "###ID_START###";
	public final static String NOTE_ID_END_IDENTIFIER = "###ID_END###";

	public final static String NOTE_TITLE_START_IDENTIFIER = "###TITLE_START###";
	public final static String NOTE_TITLE_END_IDENTIFIER = "###TITLE_END###";

	public final static String NOTE_TEXT_START_IDENTIFIER = "###TEXT_START###";
	public final static String NOTE_TEXT_END_IDENTIFIER = "###TEXT_END###";

	private static Long NEXT_NOTE_ID = 0L;
	private static String SAVE_LOCATION = "";
	private static String ARCHIVE_LOCATION = "";

	public static void setSaveLocation(final String saveLocation, final boolean updateProperties) {
		SAVE_LOCATION = saveLocation;

		if (updateProperties) {
			LifeCycle.updateProperties();
		}
	}

	public static String getSaveLocation() {
		return SAVE_LOCATION;
	}

	public static void setArchiveLocation(final String archiveLocation, final boolean updateProperties) {
		ARCHIVE_LOCATION = archiveLocation;

		if (updateProperties) {
			LifeCycle.updateProperties();
		}
	}

	public static String getArchiveLocation() {
		return ARCHIVE_LOCATION;
	}

	public static void setNextNoteId(final Long nextNoteId, final boolean updateProperties) {
		NEXT_NOTE_ID = nextNoteId;

		if (updateProperties) {
			LifeCycle.updateProperties();
		}
	}

	public static Long getNextNodeId(final boolean updateProperties, final boolean increment) {
		final Long nextNoteId = NEXT_NOTE_ID;
		if (increment) {
			NEXT_NOTE_ID++;
		}

		if (updateProperties) {
			LifeCycle.updateProperties();
		}

		return nextNoteId;
	}
}
