package org.itam.noteorganizer.internal;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import org.eclipse.e4.ui.workbench.lifecycle.PostContextCreate;

public class LifeCycle {

	private static final String SETTINGS_LOCATION = "C:\\noteorganizer\\settings.properties";
	private static final String DEFAULT_SAVE_LOCATION = "C:\\noteorganizer\\notes\\";
	private static final String DEFAULT_ARCHIVE_LOCATION = "C:\\noteorganizer\\notes\\deleted\\";

	private static final String NEXT_NOTE_ID_IDENTIFIER = "nextNodeId";
	private static final String SAVE_LOCATION_IDENTIFIER = "saveLocation";
	private static final String ARCHIVE_LOCATION_IDENTIFIER = "archiveLocation";

	@PostContextCreate
	public static void postContextCreate() {
		Properties properties = null;
		try {
			properties = new Properties();
			properties.load(new FileInputStream(SETTINGS_LOCATION));
		} catch (final FileNotFoundException e1) {
			properties = createNewPropertiesFile();
		} catch (final IOException e) {
			e.printStackTrace();
		}

		if (properties != null) {
			GlobalDefines.setNextNoteId(Long.valueOf(properties.getProperty(NEXT_NOTE_ID_IDENTIFIER)), false);
			GlobalDefines.setSaveLocation(properties.getProperty(SAVE_LOCATION_IDENTIFIER), false);
			GlobalDefines.setArchiveLocation(properties.getProperty(ARCHIVE_LOCATION_IDENTIFIER), false);
		}

		checkAndCreateNecessaryFoldersIfNecessary();
	}

	private static Properties createNewPropertiesFile() {
		try {
			final Properties properties = new Properties();
			properties.setProperty(NEXT_NOTE_ID_IDENTIFIER, "1");
			properties.setProperty(SAVE_LOCATION_IDENTIFIER, DEFAULT_SAVE_LOCATION);
			properties.setProperty(ARCHIVE_LOCATION_IDENTIFIER, DEFAULT_ARCHIVE_LOCATION);

			properties.store(new FileOutputStream(SETTINGS_LOCATION), null);

			return properties;
		} catch (final IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	private static void checkAndCreateNecessaryFoldersIfNecessary() {
		final Path path = Paths.get(GlobalDefines.getArchiveLocation());
		if ( ! path.toFile().isDirectory()) {
			path.toFile().mkdirs();
		}
	}

	public static void updateProperties() {
		try {
			final Properties properties = new Properties();
			properties.setProperty(NEXT_NOTE_ID_IDENTIFIER, GlobalDefines.getNextNodeId(false, false).toString());
			properties.setProperty(SAVE_LOCATION_IDENTIFIER, GlobalDefines.getSaveLocation());
			properties.setProperty(ARCHIVE_LOCATION_IDENTIFIER, GlobalDefines.getArchiveLocation());

			properties.store(new FileOutputStream(SETTINGS_LOCATION), null);
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

}
