package org.itam.noteorganizer.entities;

public class Note {

	private Long id;
	private String noteTitle;
	private String note;

	public Note() {
		//nothing to do here
	}

	public Note(final Long id, final String noteTitle, final String note) {
		this.id = id;
		this.noteTitle = noteTitle;
		this.note = note;
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getNoteTitle() {
		return noteTitle;
	}

	public void setNoteTitle(final String noteTitle) {
		this.noteTitle = noteTitle;
	}

	public String getNote() {
		return note;
	}

	public void setNote(final String note) {
		this.note = note;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((note == null) ? 0 : note.hashCode());
		result = (prime * result) + ((noteTitle == null) ? 0 : noteTitle.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Note other = (Note) obj;
		if (note == null) {
			if (other.note != null) {
				return false;
			}
		} else if (!note.equals(other.note)) {
			return false;
		}
		if (noteTitle == null) {
			if (other.noteTitle != null) {
				return false;
			}
		} else if (!noteTitle.equals(other.noteTitle)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Note [noteTitle=" + noteTitle + ", note=" + note + "]";
	}
}
