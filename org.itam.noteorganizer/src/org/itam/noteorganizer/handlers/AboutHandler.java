package org.itam.noteorganizer.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

public class AboutHandler {
	@Execute
	public void execute(final Shell shell) {
		MessageDialog.openInformation(shell, "About", getMessage());
	}

	private String getMessage() {
		final StringBuilder builder = new StringBuilder();

		builder.append("Note Organizer");
		builder.append(System.lineSeparator());
		builder.append(System.lineSeparator());
		builder.append("Note Organizer is a tool to organize notes. It allows simple way to create and search for notes." );

		return builder.toString();
	}
}
