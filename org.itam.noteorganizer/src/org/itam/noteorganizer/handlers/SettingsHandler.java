package org.itam.noteorganizer.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.itam.noteorganizer.wizards.SettingsWizard;

public class SettingsHandler {

	@Execute
	public void execute(final Shell shell) {
		final WizardDialog wizardDialog = new WizardDialog(shell, new SettingsWizard());

		if (wizardDialog.open() == Window.OK) {
			//TODO
		}
	}
}
