package org.itam.noteorganizer.parts;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.itam.noteorganizer.actions.DeleteNotesFromFileSystemAction;
import org.itam.noteorganizer.actions.DeleteNotesFromFileSystemAction.DeleteMode;
import org.itam.noteorganizer.actions.WriteToFileAction;
import org.itam.noteorganizer.adapter.KeyAdapter;
import org.itam.noteorganizer.adapter.SelectionAdapter;
import org.itam.noteorganizer.entities.Note;
import org.itam.noteorganizer.internal.GlobalDefines;
import org.itam.noteorganizer.internal.LifeCycle;

public class NotesDetailsPart {

	@Inject
	private MPart part;
	private MPart notesPart = null;

	@Inject
	private EPartService partService;

	//MODEL
	private Note note;


	//UI ELEMENTS
	private Text txtNoteTitle;
	private Text txtNote;

	private Button btnSave;
	private Button btnDelete;

	//Actions
	private SaveNoteAction saveNoteAction;
	private DeleteNoteAction deleteNoteAction;

	@Inject
	private MDirtyable dirty;

	@PostConstruct
	public void createComposite(final Composite parent) {
		parent.setLayout(new GridLayout(2, false));

		txtNoteTitle = new Text(parent, SWT.BORDER);
		txtNoteTitle.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		txtNoteTitle.setTextLimit(255);
		txtNoteTitle.setMessage("Insert title here");

		txtNote = new Text(parent, SWT.BORDER | SWT.MULTI);
		txtNote.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));

		btnSave = new Button(parent, SWT.PUSH);
		btnSave.setText("Save");
		btnSave.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		btnSave.setEnabled(false);

		btnDelete = new Button(parent, SWT.PUSH);
		btnDelete.setText("Delete");
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		makeActions();
		addListeners();
	}

	private void makeActions() {
		saveNoteAction = new SaveNoteAction();
		deleteNoteAction = new DeleteNoteAction();
	}

	private void addListeners() {
		txtNoteTitle.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(final KeyEvent arg0) {
				if (txtNoteTitle.getText().isEmpty()) {
					btnSave.setEnabled(false);
				} else {
					btnSave.setEnabled(true);
				}
				dirty.setDirty(true);
			}
		});

		txtNote.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(final KeyEvent arg0) {
				dirty.setDirty(true);
			}
		});

		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent arg0) {
				saveNoteAction.run();
			}
		});

		btnDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent arg0) {
				deleteNoteAction.run();
			}
		});
	}

	@Inject
	@Optional
	public void setPartInput( @Named( "input" ) final Object partInput ) {
		if (partInput instanceof Note) {
			note = (Note) partInput;
			txtNoteTitle.setText(note.getNoteTitle());
			txtNote.setText(note.getNote());
			btnSave.setEnabled(true);
		}
	}

	@Focus
	public void setFocus() {
		txtNoteTitle.setFocus();
	}

	@Persist
	public void save() {
		saveNoteAction.run();
		dirty.setDirty(false);
	}

	private class SaveNoteAction extends Action {
		@Override
		public void run() {
			if (note == null) {
				note = new Note();
			}
			if (note.getId() == null) {
				note.setId(GlobalDefines.getNextNodeId(true, true));
				LifeCycle.updateProperties();
			}
			note.setNoteTitle(txtNoteTitle.getText());
			note.setNote(txtNote.getText());
			new WriteToFileAction(note).run();

			part.setLabel(note.getNoteTitle());
			dirty.setDirty(false);

			updateNotesTableViewer();
		}
	}

	private class DeleteNoteAction extends Action {
		@Override
		public void run() {
			new DeleteNotesFromFileSystemAction(note, DeleteMode.ARCHIVE).run();

			partService.hidePart(part, true);
			updateNotesTableViewer();
		}
	}

	private void updateNotesTableViewer() {
		final MPart part = getNotesPart();
		if (part != null) {
			final NotesPart notesPart = (NotesPart) part.getObject();
			notesPart.updateTableViewer();
		}
	}

	private MPart getNotesPart() {
		if (notesPart != null) {
			return notesPart;
		}
		final Collection<MPart> col = partService.getParts();
		MPart lastOpenPart = null;
		for (final MPart mpart : col) {
			if (mpart.getContributionURI().equals(NotesPart.PART_URI)) {
				lastOpenPart = mpart;
			}
		}
		notesPart = lastOpenPart;
		return notesPart;
	}

	public Long getNoteId() {
		return note.getId();
	}
}
