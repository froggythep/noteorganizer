package org.itam.noteorganizer.parts;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MBasicFactory;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.itam.noteorganizer.actions.DeleteNotesFromFileSystemAction;
import org.itam.noteorganizer.actions.DeleteNotesFromFileSystemAction.DeleteMode;
import org.itam.noteorganizer.actions.LoadNotesFromFileSystemAction;
import org.itam.noteorganizer.adapter.SelectionAdapter;
import org.itam.noteorganizer.comparator.NoteComparator;
import org.itam.noteorganizer.entities.Note;
import org.itam.noteorganizer.filter.TextFilter;
import org.itam.noteorganizer.provider.NoteLabelProvider;
import org.itam.noteorganizer.util.GUIUtil;

public class NotesPart {

	public final static String PART_URI = "bundleclass://org.itam.noteorganizer/org.itam.noteorganizer.parts.NotesPart";

	//UI-Elements
	private Text txtFilter;
	private TableViewer tblViewerNotes;
	private Button btnReloadNotes;

	//ACTIONS
	private NewNoteAction newNoteAction;
	private DeleteNoteAction deleteNoteAction;
	private OpenNoteAction openNoteAction;

	private LoadNotesFromFileSystemAction loadNotesAction;

	//MISC
	TextFilter filter = new TextFilter();

	@Inject
	MApplication application;

	@Inject
	EPartService partService;

	@Inject
	EModelService modelService;


	@PostConstruct
	public void createComposite(final Composite parent) {
		parent.setLayout(new GridLayout(1, false));

		txtFilter = new Text(parent, SWT.BORDER);
		txtFilter.setMessage("Enter a text to filter Notes");
		txtFilter.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		tblViewerNotes = new TableViewer(parent);
		final GridData tblViewerNotesGridData = new GridData(SWT.FILL, SWT.FILL, false, true);
		tblViewerNotesGridData.widthHint = 100;
		tblViewerNotes.getTable().setLayoutData(tblViewerNotesGridData);
		tblViewerNotes.setContentProvider(ArrayContentProvider.getInstance());
		tblViewerNotes.setLabelProvider(new NoteLabelProvider());
		tblViewerNotes.setComparator(new NoteComparator());
		tblViewerNotes.addFilter(filter);

		btnReloadNotes = new Button(parent, SWT.PUSH);
		btnReloadNotes.setText("Reload Notes");
		btnReloadNotes.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		makeActions();
		hookContextMenu();
		addListeners();
		initData();
	}

	private void makeActions() {
		newNoteAction = new NewNoteAction();
		deleteNoteAction = new DeleteNoteAction();
		openNoteAction = new OpenNoteAction();

		loadNotesAction = new LoadNotesFromFileSystemAction();
	}

	private void hookContextMenu() {
		GUIUtil.hookContextMenu(tblViewerNotes, openNoteAction, newNoteAction, deleteNoteAction);
	}

	private void addListeners() {
		txtFilter.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(final KeyEvent e) {
				filter.setSearchText(txtFilter.getText());
				tblViewerNotes.refresh();
			}
		});

		tblViewerNotes.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(final SelectionChangedEvent arg0) {
				openNoteAction.setEnabled(openNoteAction.isEnabled());
				deleteNoteAction.setEnabled(deleteNoteAction.isEnabled());
				hookContextMenu();
			}
		});

		tblViewerNotes.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(final DoubleClickEvent arg0) {
				openNoteAction.run();
			}
		});

		btnReloadNotes.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(final SelectionEvent arg0) {
				initData();
			}
		});
	}

	private void initData() {
		loadNotesAction.run();
		tblViewerNotes.setInput(loadNotesAction.getNotes());
	}

	@Focus
	public void setFocus() {
		tblViewerNotes.getTable().setFocus();
	}

	public void updateTableViewer() {
		initData();
	}

	private MPart showNewNotesDetailsPart(final String labelName) {
		final MPart part = MBasicFactory.INSTANCE.createPart();
		part.setLabel(labelName);
		part.setContributionURI("bundleclass://org.itam.noteorganizer/org.itam.noteorganizer.parts.NotesDetailsPart");
		part.setCloseable(true);
		final List<MPartStack> stacks = modelService.findElements(application, null, MPartStack.class, null);
		stacks.get(1).getChildren().add(part);
		partService.showPart(part, PartState.ACTIVATE);

		return part;
	}

	private class NewNoteAction extends Action {
		@Override
		public String getText() {
			return "New Note";
		}

		@Override
		public void run() {
			showNewNotesDetailsPart("New Note");
		}
	}

	private class DeleteNoteAction extends Action {

		@Override
		public boolean isEnabled() {
			return !tblViewerNotes.getStructuredSelection().isEmpty();
		}

		@Override
		public String getText() {
			return "Delete Note";
		}

		@Override
		public void run() {
			final Collection<Note> notesToDelete = new HashSet<>(tblViewerNotes.getStructuredSelection().size());
			tblViewerNotes.getStructuredSelection().iterator().forEachRemaining(note -> {
				notesToDelete.add((Note) note);
			});
			new DeleteNotesFromFileSystemAction(notesToDelete, DeleteMode.ARCHIVE).run();

			initData();
		}
	}

	private class OpenNoteAction extends Action {
		@Override
		public boolean isEnabled() {
			return !tblViewerNotes.getStructuredSelection().isEmpty();
		}

		@Override
		public String getText() {
			return "Open Note";
		}

		@Override
		public void run() {
			final Note selectedNote = (Note) tblViewerNotes.getStructuredSelection().getFirstElement();
			final MPart openPart = getPartIfNoteAlreadyOpen(selectedNote);
			if ( openPart == null) {
				final MPart part = showNewNotesDetailsPart(selectedNote.getNoteTitle());

				final Collection<MPart> col = partService.getParts();
				MPart lastOpenPart = null;
				for (final MPart mpart : col) {
					if (mpart.getContributionURI().equals(part.getContributionURI())) {
						lastOpenPart = mpart;
					}
				}
				if (lastOpenPart != null) {
					lastOpenPart.getContext().modify("input", selectedNote);
				}
			} else {
				partService.bringToTop(openPart);
			}
		}

		private MPart getPartIfNoteAlreadyOpen(final Note selectedNote) {
			final Collection<MPart> openParts = partService.getParts();
			for (final MPart mPart : openParts) {
				if (mPart.getObject() instanceof NotesDetailsPart) {
					final NotesDetailsPart part = (NotesDetailsPart) mPart.getObject();
					if ((part.getNoteId() != null) && part.getNoteId().equals(selectedNote.getId())) {
						return mPart;
					}
				}
			}

			return null;
		}
	}

}