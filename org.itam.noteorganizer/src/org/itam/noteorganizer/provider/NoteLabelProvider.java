package org.itam.noteorganizer.provider;

import org.eclipse.jface.viewers.LabelProvider;
import org.itam.noteorganizer.entities.Note;

public class NoteLabelProvider extends LabelProvider {

	@Override
	public String getText(Object element) {
		if (element instanceof Note) {
			return ((Note) element).getNoteTitle();
		}
		return "";
	}
}
