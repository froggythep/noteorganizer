package org.itam.noteorganizer.comparator;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.itam.noteorganizer.entities.Note;

public class NoteComparator extends ViewerComparator {

	@Override
	public int compare(Viewer viewer, Object note1, Object note2) {
		if (note1 instanceof Note && note2 instanceof Note) {
			return ((Note) note1).getNoteTitle().compareTo(((Note) note2).getNoteTitle());
		}
		return 0;
	}

}
